import server from '../src/server'
import request from 'supertest'
import { jest } from '@jest/globals'
import { pool } from '../src/db'

const initializeMockPool = (mockResponse: any) => {
    (pool as any).connect = jest.fn(() => {
        return {
            query: () => mockResponse,
            release: () => null,
        }
    })
}


// Comments tests

describe('Testing  /comments', () => {
  const mockResponse = {
    rows: [
      {
        comment_id: 1,
        user_id: 1,
        post_id: 1,
        content: "buu",
        comment_date: new Date().toString(),
      },
      {
        comment_id: 2,
        user_id: 1,
        post_id: 2,
        content: "baah",
        comment_date: new Date().toString(),
      },
    ],
  };
  beforeAll(() => {
      initializeMockPool(mockResponse)
  })
  afterAll(() => {
      jest.clearAllMocks()
  })


  it('get/comments/:UserId returns comments by userId ', async () => {
      
      const response = await request(server).get('/comments/1')
      expect(response.body).toStrictEqual(mockResponse.rows)
  })
 
  it('post/comments/ posts new comment and returns it back ', async () => {
      
      const response = await request(server).post('/comments/')
      .send({user_id: 1, post_id: 1, content: "bleh"})
      expect(response.body).toStrictEqual(mockResponse.rows[0])
  })

  it('post/comments/ posts new comment and returns it back ', async () => {
      
      const response = await request(server).post('/comments/')
      .send({user_id: 1, post_id: 1, content: "bleh"})
      expect(response.body).toStrictEqual(mockResponse.rows[0])
  })

  it('put/comments/:id modifies content from comment ', async () => {
      
      const response = await request(server).put('/comments/1')
      .send({content: "I changed"})
      expect(response.body).toStrictEqual({content: "I changed"})
  })

  it('delete/comments/:id deletes comment by id ', async () => {
      
      const response = await request(server).delete('/comments/1')
      expect(response.body).toStrictEqual({id: 1, message: "deleted"})
  })

})